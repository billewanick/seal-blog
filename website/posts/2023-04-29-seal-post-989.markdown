---
title: Seal Post Number 989
---

Check out this magnetizing, plump seal!
<img
  src="/images/seal29.jpg"
  alt="A picture of a magnetizing, plump seal! <3"
  width="400"
/>
