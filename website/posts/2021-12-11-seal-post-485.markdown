---
title: Seal Post Number 485
---

Look upon and tremble at this winsome, ample seal!
<img
  src="/images/seal18.jpg"
  alt="A picture of a winsome, ample seal! <3"
  width="400"
/>
