---
title: Seal Post Number 277
---

Look upon and tremble at this pretty, husky seal!
<img
  src="/images/seal49.jpg"
  alt="A picture of a pretty, husky seal! <3"
  width="400"
/>
