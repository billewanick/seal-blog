---
title: Seal Post Number 90
---

Check out this dainty, podgy seal!
<img
  src="/images/seal8.jpg"
  alt="A picture of a dainty, podgy seal! <3"
  width="400"
/>
