---
title: Seal Post Number 6
---

Look upon and tremble at this magnetizing, buxom seal!
<img
  src="/images/seal4.jpg"
  alt="A picture of a magnetizing, buxom seal! <3"
  width="400"
/>
