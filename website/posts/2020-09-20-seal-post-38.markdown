---
title: Seal Post Number 38
---

Look upon and tremble at this delightful, hefty seal!
<img
  src="/images/seal68.jpg"
  alt="A picture of a delightful, hefty seal! <3"
  width="400"
/>
