---
title: Seal Post Number 1027
---

Whoa! See this delicate, bearish seal!
<img
  src="/images/seal52.jpg"
  alt="A picture of a delicate, bearish seal! <3"
  width="400"
/>
