---
title: Seal Post Number 601
---

Check out this charming, full-figured seal!
<img
  src="/images/singerSeal.jpg"
  alt="A picture of a charming, full-figured seal! <3"
  width="400"
/>
