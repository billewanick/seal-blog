---
title: Seal Post Number 369
---

Check out this magnetizing, plump seal!
<img
  src="/images/seal25.jpg"
  alt="A picture of a magnetizing, plump seal! <3"
  width="400"
/>
