---
title: Seal Post Number 1003
---

Gaze upon this suave, pleasingly plump seal!
<img
  src="/images/seal68.jpg"
  alt="A picture of a suave, pleasingly plump seal! <3"
  width="400"
/>
