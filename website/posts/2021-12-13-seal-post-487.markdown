---
title: Seal Post Number 487
---

Look upon and tremble at this charming, hefty seal!
<img
  src="/images/seal43.jpg"
  alt="A picture of a charming, hefty seal! <3"
  width="400"
/>
