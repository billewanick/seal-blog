---
title: Seal Post Number 451
---

Look at this darling, pleasingly plump seal!
<img
  src="/images/singerSeal.jpg"
  alt="A picture of a darling, pleasingly plump seal! <3"
  width="400"
/>
