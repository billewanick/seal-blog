---
title: Seal Post Number 257
---

Look at this ravishing, plumpish seal!
<img
  src="/images/seal64.jpg"
  alt="A picture of a ravishing, plumpish seal! <3"
  width="400"
/>
