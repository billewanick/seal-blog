---
title: Seal Post Number 963
---

Look upon and tremble at this graceful, fatty seal!
<img
  src="/images/seal39.jpg"
  alt="A picture of a graceful, fatty seal! <3"
  width="400"
/>
