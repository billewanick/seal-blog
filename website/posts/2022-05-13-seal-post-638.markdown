---
title: Seal Post Number 638
---

Look upon and tremble at this heavenly, portly seal!
<img
  src="/images/seal2.jpg"
  alt="A picture of a heavenly, portly seal! <3"
  width="400"
/>
