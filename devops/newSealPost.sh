#!/bin/sh
HOME="/home/alice/seal-blog"

# Call generateSealPosts from root of blog
#   This will generate posts up to today
cd $HOME
./generate/generateSealPosts

# cd to website so site can find posts folder and build
cd $HOME/website

# Before our posts will show up on the site
# we need to build them so they're generated
# from our site.hs logic
./site build

# Push to git
git config --global user.email "admin@cutesealfanpage.love"
git config --global user.name "Alice"

cd $HOME
ssh-add
git add .
git commit -m "Daily blog update"
git push origin master
