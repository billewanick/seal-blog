# Builds the site using the ghc and hakyll setup in shell.nix
nix-shell --pure --command 'ghc --make website/site -outputdir dist -static'
